package EnvironmentandRepositories;

import java.io.IOException;
import java.util.ArrayList;

import CommonMethods.Utilities;

public class RequestRepository extends Environment {
	
	public static String post_param_requestBody(String testcaseName) throws IOException {

		ArrayList<String> data = Utilities.ReadExceldata("Post_API", testcaseName);
		String req_name = data.get(1);
		String req_job = data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \""+ req_name +"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		return requestBody;
	}
	
	public static String post_requestbody()
	{
		String requestbody = "{\r\n" + "\"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		return requestbody;
	}
	public static String put_requestbody()
	{
		String requestbody = "{\r\n"
				+ "    \"name\": \"Rohini\",\r\n"
				+ "    \"job\": \"BA\"\r\n"
				+ "}";
		return requestbody;
	}
	public static String patch_requestbody()
	{
		String requestbody = "{\r\n"
				+ "    \"name\": \"Jhon\",\r\n"
				+ "    \"job\": \"Developer\"\r\n"
				+ "}";
		return requestbody;
	}
	public static String get_requestbody()
	{
		String requestbody = "";
		return requestbody;
	}
	public static String delete_requestbody()
	{
		String requestbody = "";
		return requestbody;
	}

}
