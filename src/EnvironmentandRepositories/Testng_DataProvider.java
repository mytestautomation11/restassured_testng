package EnvironmentandRepositories;

import org.testng.annotations.DataProvider;

public class Testng_DataProvider {
	
	@DataProvider()
	public Object[][] requestbody() {
		return new Object[][] { { "morpheus", "Leader" }, { "Roshni", "Tester" } };
	}


}
