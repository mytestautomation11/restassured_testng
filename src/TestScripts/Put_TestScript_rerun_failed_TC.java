package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.Testng_Retry_analyzer;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_TestScript_rerun_failed_TC extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest 
	public void setup() {
		logfolder = Utilities.createfolder("Put_API");
	}

	@Test (retryAnalyzer = Testng_Retry_analyzer.class, description = "Validate the responseBody parameters of Put_TC_1")
	public void validate_Put1() {

		response = Put_API_Trigger(put_requestbody(), put_endpoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		// String res_id = responseBody.jsonPath().getString("id");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(put_requestbody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		// Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_updatedAt, expecteddate, "updatedAT in ResponseBody is not equal to Date Generated");

	}
	
	@Test (retryAnalyzer = Testng_Retry_analyzer.class, description = "Validate the responseBody parameters of Put_TC_1")
	public void validate_Put2() {

		response = Put_API_Trigger(put_requestbody(), put_endpoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		// String res_id = responseBody.jsonPath().getString("id");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(put_requestbody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		// Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_updatedAt, expecteddate, "updatedAT in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Put_API_TC1", logfolder, put_endpoint(), put_requestbody(),
				response.getHeaders().toString(), responseBody.asString());
	}

}
