package TestScripts;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.Testng_Retry_analyzer;
import CommonMethods.Utilities;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Get_TestScript extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createfolder("Get_API");
	}

	@Test (retryAnalyzer = Testng_Retry_analyzer.class , description = "Validate the responseBody parameters of Get_TC_1")
	public void validate_Get() {

		response = Get_API_Trigger(get_requestbody(), get_endpoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();

		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		// declare the expected results from data array

		int[] exp_id = { 7, 8, 9, 10, 11, 12 };

		String[] exp_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };

		String[] exp_first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };

		String[] exp_last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

		String[] exp_avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		// Declare expected results of support
		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		// Fetch page parameters

		int res_page = responseBody.jsonPath().getInt("page");
		int res_per_page = responseBody.jsonPath().getInt("per_page");
		int res_total = responseBody.jsonPath().getInt("total");
		int res_total_pages = responseBody.jsonPath().getInt("total_pages");

		// Fetch Size of data array
		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();

		// Validation
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
		// Validate per page parameters
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);

		// Validate data array
		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), exp_id[i],
					"Validation of id failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), exp_email[i],
					"Validation of email failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].first_name"), exp_first_name[i],
					"Validation of first_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].last_name"), exp_last_name[i],
					"Validation of last_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].avatar"), exp_avatar[i],
					"Validation of avatar failed for json object at index : " + i);
		}

		// Step 7.3 : Validate support json
		Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");
		Assert.assertEquals(responseBody.jsonPath().getString("support.text"), exp_text, "Validation of URL failed");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Get_API_TC1", logfolder, get_endpoint(), get_requestbody(),
				response.getHeaders().toString(), responseBody.asString());
	}

}
