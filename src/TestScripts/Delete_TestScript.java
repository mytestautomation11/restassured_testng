package TestScripts;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.Testng_Retry_analyzer;
import CommonMethods.Utilities;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_TestScript extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createfolder("Delete_API");
	}
	
	@Test (retryAnalyzer = Testng_Retry_analyzer.class , description = "Validate the responseBody parameters of Delete_TC_1")
	public void execute_Delete() throws IOException {

		response = Delete_API_Trigger(delete_requestbody(), delete_endpoint());
		int status_code = response.statusCode();
		
		Assert.assertEquals(status_code, 204, "Correct status code not found even after retrying for 5 times");
		}

	
	
	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Delete_API_TC1", logfolder, delete_endpoint(), delete_requestbody(),
				response.getHeaders().toString(), response.getBody().asString());
	}

}
